STI-IT LDAP authentication
==========================

This roles sets up LDAP authentication at EPFL on Debian based machines.

Requirements
------------

The role has been tested only on Ubuntu 18.04.

Role Variables
--------------

There is only one configurable variable that can be used: `sssd_simple_group_access`
This variable is used to update the `sssd.conf` in order to manage which LDAP group is allowed to authenticate on the machine.

Dependencies
------------

No specific dependency.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

```yml
---
- hosts: all
  roles:
    - sti_it.ldap_authentication
  become: yes
```

License
-------

BSD

Author Information
------------------

Emmanuel Jaep (emmanuel dot jaep at epfl dot ch).
